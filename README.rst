====================
MDE Sphinx Extension
====================


.. image:: https://img.shields.io/pypi/v/mde_sphinx_ext.svg
        :target: https://pypi.python.org/pypi/mde-sphinx-ext

.. image:: https://readthedocs.org/projects/mde-sphinx-ext/badge/?version=latest
        :target: https://mde-sphinx-ext.readthedocs.io/en/latest/?version=latest
        :alt: Documentation Status




Sphinx extension for the model data explorer


* Documentation: https://mde-sphinx-ext.readthedocs.io.
* Free software: EUPL-1.2 License


.. note::

    This project is under active development and has been generated with
    https://github.com/audreyfeldroy/cookiecutter-pypackage. Not all
    features, including docs and the package are implemented yet.
