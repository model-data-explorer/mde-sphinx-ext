.. highlight:: shell

============
Installation
============


Stable release
--------------

To install MDE Sphinx Extension, run this command in your terminal:

.. code-block:: console

    $ pip install mde-sphinx-ext

This is the preferred method to install MDE Sphinx Extension, as it will always install the most recent stable release.

If you don't have `pip`_ installed, this `Python installation guide`_ can guide
you through the process.

.. _pip: https://pip.pypa.io
.. _Python installation guide: http://docs.python-guide.org/en/latest/starting/installation/


From sources
------------

The sources for MDE Sphinx Extension can be downloaded from the `Gitlab repo`_.

You can either clone the public repository:

.. code-block:: console

    $ git clone git://gitlab.hzdr.de/model-data-explorer/mde-sphinx-ext

Once you have a copy of the source, you can install it with:

.. code-block:: console

    $ python setup.py install


.. _Gitlab repo: https://gitlab.hzdr.de/model-data-explorer/mde_sphinx_ext
