"""MDE Sphinx Extension."""

# Disclaimer
# ----------
#
# Copyright (C) 2022 Helmholtz-Zentrum Hereon
#
# This file is part of mde-sphinx-ext and is released under the
# EUPL-1.2 license.
# See LICENSE in the root of the repository for full licensing details.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the EUROPEAN UNION PUBLIC LICENCE v. 1.2 or later
# as published by the European Commission.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# EUPL-1.2 license for more details.
#
# You should have received a copy of the EUPL-1.2 license along with this
# program. If not, see https://www.eupl.eu/.
# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use osp.abspath to make it absolute, like shown here.
#
import os
import os.path as osp
import shlex
import shutil
import subprocess as spr
import sys
import tempfile
from itertools import starmap
from textwrap import indent

from docutils import nodes
from docutils.statemachine import StringList
from sphinx.directives import SphinxDirective
from sphinx.ext.graphviz import Graphviz

from . import _version

__author__ = """Philipp S. Sommer"""
__email__ = "philipp.sommer@hereon.de"
__version__ = "0.1.0"


__version__ = _version.get_versions()["version"]


class DjangoGraphviz(Graphviz):
    """Direction to render a Django model description as graph."""

    option_spec = Graphviz.option_spec
    option_spec["options"] = str

    def run(self):

        if "align" in self.options:
            if self.env.app.builder.name == "latex":
                del self.options["align"]

        with tempfile.TemporaryDirectory() as tmpdir:
            workdir = osp.join(tmpdir, "django_templateapp")
            shutil.copytree(
                osp.join(osp.dirname(__file__), "django_templateapp"), workdir
            )
            appdir = osp.join(workdir, "templateapp")
            with open(osp.join(appdir, "models.py"), "a") as f:
                f.write("\n".join(self.content))
            base_graph_file = "tmp_graph.dot"
            graph_file = osp.join(
                self.state.document.settings.env.srcdir, base_graph_file
            )
            options = self.options.get("options")
            if options:
                options = shlex.split(options)
            else:
                options = []
            try:
                spr.check_output(
                    [
                        sys.executable,
                        osp.join(workdir, "manage.py"),
                        "graph_models",
                        "--output",
                        graph_file,
                        "templateapp",
                    ]
                    + options
                )
                self.content = self.content[:0]
                self.arguments.append("/" + base_graph_file)
                graph_nodes = super().run()
            except Exception:
                raise
            finally:
                os.remove(graph_file)
        return graph_nodes


DJANGO_SOURCE_GRAPH_RST = """
.. tab-set::

    .. tab-item:: Graph
        :sync: django-source-graph-graph

        .. django-graph::
{options}

{content}

    .. tab-item:: Django source
        :sync: django-source-graph-source

        .. code-block:: python

{content}
"""


class DjangoSourceDirective(SphinxDirective):
    """A directive to display a graph and it's source code.

    Notes
    -----
    requires the ``sphinx_design`` extension to be installed!
    """

    option_spec = Graphviz.option_spec

    has_content = True

    def run(self):
        content = indent("\n".join(self.content), " " * 12)
        options = indent(
            "\n".join(starmap(":{}: {}".format, self.options.items())),
            " " * 12,
        )
        rendered_rst = DJANGO_SOURCE_GRAPH_RST.format(
            content=content, options=options
        )
        new_content = StringList(rendered_rst.splitlines())
        paragraph = nodes.paragraph()
        self.state.nested_parse(new_content, 0, paragraph)
        return [paragraph[0]]


def setup(app):
    """Add the role to link to gitlab issues."""

    app.add_directive("django-graph", DjangoGraphviz)
    app.add_directive("django-source-graph", DjangoSourceDirective)

    return {
        "version": "0.0.1",
        "parallel_write_safe": False,
        "parallel_read_safe": False,
    }
